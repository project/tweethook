<?php

/**
 * @file
 * Admin setting form for TweetHook module.
 */

/**
 * Implementation of hook_admin_settings().
 */
function tweethook_admin_settings() {
  $form = array();
  global $base_url;
  $username = variable_get('tweethook_api_username', '');
  $password = variable_get('tweethook_api_password', '');
  $verification = variable_get('tweethook_verification_key', '');
  $collapsed = FALSE;
  if (!empty($username) || !empty($password) || !empty($verification)) {
    $collapsed = TRUE;
  }
  $form['tweethook_callback_url'] = array(
    '#type' => 'item',
    '#title' => t('Callback URL'),
    '#value' => "{$base_url}/tweethook/callback",
  );
  $form['api_credentials'] = array(
    '#type' => 'fieldset',
    '#title' => t('API Credentials'),
    '#collapsible' => TRUE,
    '#collapsed' => $collapsed,
  );
  $form['api_credentials']['tweethook_api_username'] = array(
    '#type' => 'textfield',
    '#title' => t('API Username'),
    '#default_value' => variable_get('tweethook_api_username', ''),
  );
  $form['api_credentials']['tweethook_api_password'] = array(
    '#type' => 'textfield',
    '#title' => t('API Password'),
    '#default_value' => variable_get('tweethook_api_password', ''),
  );
  $form['api_credentials']['tweethook_verification_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Postdata verification key'),
    '#default_value' => variable_get('tweethook_verification_key', ''),
  );
  return system_settings_form($form);
}
